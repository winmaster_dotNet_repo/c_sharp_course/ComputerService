USE [master]
GO
/****** Object:  Database [KatarzynaBialasComputerServiceLab3]    Script Date: 2016-04-27 14:30:25 ******/
CREATE DATABASE [KatarzynaBialasComputerServiceLab3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'KatarzynaBialasComputerServiceLab3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\KatarzynaBialasComputerServiceLab3.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'KatarzynaBialasComputerServiceLab3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\KatarzynaBialasComputerServiceLab3_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [KatarzynaBialasComputerServiceLab3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET ARITHABORT OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET  DISABLE_BROKER 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET  MULTI_USER 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET DELAYED_DURABILITY = DISABLED 
GO
USE [KatarzynaBialasComputerServiceLab3]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 2016-04-27 14:30:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[ComputerID] [int] NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Computers]    Script Date: 2016-04-27 14:30:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Computers](
	[ID] [int] NOT NULL,
	[Manufacturer] [nvarchar](50) NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
	[RAM] [int] NOT NULL,
 CONSTRAINT [PK_Computers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Services]    Script Date: 2016-04-27 14:30:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[ID] [int] NOT NULL,
	[WorkerID] [int] NOT NULL,
	[ComputerID] [int] NOT NULL,
	[Cost] [int] NOT NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workers]    Script Date: 2016-04-27 14:30:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Worker] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (1, N'Jan', N'Kowalski', 1)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (2, N'Artur', N'Młynarski', 2)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (3, N'Edward', N'Nożyce', 3)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (4, N'Julita', N'Kobuc', 4)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (5, N'Jan', N'Kowalski', 5)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (6, N'Artur', N'Młynarski', 6)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (7, N'Artur', N'Młynarski', 7)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (8, N'Aleksandra', N'Pomagierska', 8)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (9, N'Jakub', N'Nadwoźny', 9)
INSERT [dbo].[Clients] ([ID], [Name], [Surname], [ComputerID]) VALUES (10, N'Jarosław', N'Domagała', 10)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (1, N'Lenovo', N'SVN', 1)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (2, N'Dell', N'Latitude', 8)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (3, N'Samsung', N'SRF', 2)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (4, N'Fujitsu', N'gfg125', 2)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (5, N'Asus', N'Sus', 5)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (6, N'MSI', N'gxt4', 4)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (7, N'Apple', N'MacX', 1)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (8, N'Tracer', N'Econo', 4)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (9, N'Sony', N'Vaio', 4)
INSERT [dbo].[Computers] ([ID], [Manufacturer], [Model], [RAM]) VALUES (10, N'Toshiba', N'Satelite', 2)
INSERT [dbo].[Services] ([ID], [WorkerID], [ComputerID], [Cost]) VALUES (1, 1, 1, 500)
INSERT [dbo].[Services] ([ID], [WorkerID], [ComputerID], [Cost]) VALUES (2, 2, 2, 15000)
INSERT [dbo].[Services] ([ID], [WorkerID], [ComputerID], [Cost]) VALUES (3, 5, 10, 25)
INSERT [dbo].[Services] ([ID], [WorkerID], [ComputerID], [Cost]) VALUES (4, 3, 5, 75)
INSERT [dbo].[Services] ([ID], [WorkerID], [ComputerID], [Cost]) VALUES (5, 4, 7, 650)
SET IDENTITY_INSERT [dbo].[Workers] ON 

INSERT [dbo].[Workers] ([ID], [Name], [Surname]) VALUES (1, N'Andrzej', N'Pececik')
INSERT [dbo].[Workers] ([ID], [Name], [Surname]) VALUES (2, N'Jerzy', N'Klawiatura')
INSERT [dbo].[Workers] ([ID], [Name], [Surname]) VALUES (3, N'Robert', N'Ramerski')
INSERT [dbo].[Workers] ([ID], [Name], [Surname]) VALUES (4, N'Janusz', N'Dyskowy')
INSERT [dbo].[Workers] ([ID], [Name], [Surname]) VALUES (5, N'Artur', N'Magistrala')
SET IDENTITY_INSERT [dbo].[Workers] OFF
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Computers] FOREIGN KEY([ComputerID])
REFERENCES [dbo].[Computers] ([ID])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Computers]
GO
ALTER TABLE [dbo].[Services]  WITH CHECK ADD  CONSTRAINT [FK_Services_Computers] FOREIGN KEY([ComputerID])
REFERENCES [dbo].[Computers] ([ID])
GO
ALTER TABLE [dbo].[Services] CHECK CONSTRAINT [FK_Services_Computers]
GO
ALTER TABLE [dbo].[Services]  WITH CHECK ADD  CONSTRAINT [FK_Services_Workers] FOREIGN KEY([WorkerID])
REFERENCES [dbo].[Workers] ([ID])
GO
ALTER TABLE [dbo].[Services] CHECK CONSTRAINT [FK_Services_Workers]
GO
USE [master]
GO
ALTER DATABASE [KatarzynaBialasComputerServiceLab3] SET  READ_WRITE 
GO
