﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasComputerServiceLab3.Controller
{
   public  class Service
    {
        public static void GetAllServices(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("select s.ID, c.Model, c.Manufacturer as Producent, w.Surname as Nazwisko,  s.Cost as Koszt from Workers w join Services s on s.WorkerID = w.ID join Computers c on s.ComputerID = c.ID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetService(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, int cost)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("select s.ID, c.Model, c.Manufacturer as Producent, w.Surname as Nazwisko,  s.Cost as Koszt from Workers w join Services s on s.WorkerID = w.ID join Computers c on s.ComputerID = c.ID where Cost >" + cost, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllComputers(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID, Manufacturer as Producent, Model, RAM from Computers", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllWorkers(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID, Name as Imię, Surname as Nazwisko from Workers", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllClients(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID as lp, Name as Imię, Surname as Nazwisko from Clients", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllClientsComputers(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select a.ID as lp, a.Name as Imie, a.Surname as Nazwisko, b.Manufacturer as Producent, b.Model from Clients a join Computers b on a.ComputerID = b.ID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetClientsFromService(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select a.Name as Imię, a.Surname as Nazwisko, b.Manufacturer as Producent, b.Model, c.ID as Nr_Serwisu, c.Cost as Koszt from Clients a join Computers b on b.ID = a.ComputerID join Services c on c.ComputerID = a.ComputerID", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllComputersFromClientName(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, string name, string surname)
        {
            dataGridView.DataSource = null;
            string query= "Select a.Name as Imie, a.Surname as Nazwisko, b.Manufacturer as Producent, b.Model from Clients a join Computers b on a.ComputerID = b.ID where Name = '"+name+"' and Surname = '"+surname+"'";
            sqlDataAdapter = new SqlDataAdapter(query, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
    }
}
